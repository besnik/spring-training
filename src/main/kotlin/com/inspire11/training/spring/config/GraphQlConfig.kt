package com.inspire11.training.spring.config

import com.inspire11.training.spring.GraphQLErrorAdapter
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import graphql.ExceptionWhileDataFetching
import graphql.GraphQLError
import java.util.ArrayList
import java.util.stream.Collectors
import graphql.servlet.GraphQLErrorHandler


@Configuration
class GraphQLConfig {
    @Bean
    fun errorHandler(): GraphQLErrorHandler {
        return object : GraphQLErrorHandler {
            override fun processErrors(errors: List<GraphQLError>): List<GraphQLError> {
                val clientErrors = errors.stream()
                        .filter { it -> isClientError(it) }
                        .collect(Collectors.toList())

                val serverErrors = errors.stream()
                        .filter { it -> !isClientError(it) }
                        .map { it -> GraphQLErrorAdapter(it) }
                        .collect(Collectors.toList())

                val e = ArrayList<GraphQLError>()
                e.addAll(clientErrors)
                e.addAll(serverErrors)
                return e
            }

            protected fun isClientError(error: GraphQLError): Boolean {
                return !(error is ExceptionWhileDataFetching || error is Throwable)
            }
        }
    }
}
