package com.inspire11.training.spring.controller

import com.inspire11.training.spring.graphql.dto.ArticleDto
import com.inspire11.training.spring.graphql.service.ArticleService
import com.inspire11.training.spring.model.Article
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api")
class ArticleController(private val articleService: ArticleService) {

    @GetMapping("/articles")
    fun getAllArticles(): List<ArticleDto> =
            articleService.findAllArticles().map { ArticleDto(it) }

    @PostMapping("/articles")
    fun createNewArticle(@Valid @RequestBody article: ArticleDto): ArticleDto =
            ArticleDto(articleService.createArticle(
                    authorId = article.authorId!!,
                    title = article.title!!,
                    content = article.content!!
            ))


    @GetMapping("/articles/{id}")
    fun getArticleById(@PathVariable(value = "id") articleId: Long): ArticleDto =
            ArticleDto(articleService.findArticle(articleId))

    @PutMapping("/articles/{id}")
    fun updateArticleById(@PathVariable(value = "id") articleId: Long,
                          @Valid @RequestBody newArticle: ArticleDto): ResponseEntity<ArticleDto> {

        val a = articleService.updateArticle(
                articleId = articleId,
                title = newArticle.title!!,
                content = newArticle.title!!
        )
        return ResponseEntity.ok().body(ArticleDto(a))
    }

    @DeleteMapping("/articles/{id}")
    fun deleteArticleById(@PathVariable(value = "id") articleId: Long): ResponseEntity<Void> {
        articleService.deleteArticle(articleId)
        return ResponseEntity<Void>(HttpStatus.OK)
    }
}
