package com.inspire11.training.spring.model

import javax.persistence.*
import javax.validation.constraints.NotBlank

@Entity
data class Article (
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null,

        @get: NotBlank
        var title: String? = null,

        @get: NotBlank
        var content: String? = null
) {
    @ManyToOne(optional = false, cascade = [CascadeType.ALL])
    var author: Author? = null
}
