package com.inspire11.training.spring.model

import javax.persistence.*
import javax.validation.constraints.NotBlank

@Entity
data class Author (
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null,

        @get: NotBlank
        var name: String? = null,

        @get: NotBlank
        var surname: String? = null
) {
        @OneToMany(mappedBy = "author")
        var articles: Set<Article> = HashSet<Article>()
}
