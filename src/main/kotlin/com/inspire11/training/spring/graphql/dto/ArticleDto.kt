package com.inspire11.training.spring.graphql.dto

import com.inspire11.training.spring.model.Article

data class ArticleDto(
        var id: Long? = null,
        var title: String? = null,
        var content: String? = null,
        var author: AuthorDto? = null,
        var authorId: Long? = null
) {
    constructor(article: Article) : this() {
        id = article.id
        title = article.title
        content = article.content
        author = AuthorDto(article.author!!)
    }
}