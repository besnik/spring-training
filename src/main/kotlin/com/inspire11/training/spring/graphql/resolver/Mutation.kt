package com.inspire11.training.spring.graphql.resolver

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import org.springframework.stereotype.Component

@Component
class Mutation : GraphQLMutationResolver {
    fun version() = "1.0.0"
}
