package com.inspire11.training.spring.graphql.service

import com.inspire11.training.spring.model.Article
import com.sun.org.apache.xpath.internal.operations.Bool

interface ArticleService {
    fun findAllArticles(): List<Article>
    fun findArticle(id: Long): Article
    fun createArticle(authorId: Long, title: String, content: String): Article
    fun updateArticle(articleId: Long, title: String, content: String): Article
    fun deleteArticle(articleId: Long): Boolean
    fun changeAuthor(articleId: Long, authorId: Long): Article
}