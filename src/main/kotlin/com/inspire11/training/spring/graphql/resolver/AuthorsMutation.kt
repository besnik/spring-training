package com.inspire11.training.spring.graphql.resolver

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import org.springframework.stereotype.Component
import com.inspire11.training.spring.graphql.dto.AuthorDto
import com.inspire11.training.spring.model.Author
import com.inspire11.training.spring.repository.AuthorRepository
import javax.transaction.Transactional

@Component
class AuthorsMutation(private val authorRepository: AuthorRepository) : GraphQLMutationResolver {
    @Transactional
    fun createAuthor(author: AuthorDto): AuthorDto? {
        val a = Author(
                name = author.name!!,
                surname = author.surname!!
        )
        authorRepository.save(a)
        return AuthorDto(a)
    }

    fun updateAuthor(authorId: Long, author: AuthorDto): AuthorDto? {
        val a = authorRepository.getOne(authorId)
        a.name = author.name!!
        a.surname = author.surname!!
        authorRepository.save(a)
        return AuthorDto(a)
    }

    fun deleteAuthor(authorId: Long): Boolean {
        authorRepository.deleteById(authorId)
        return true
    }
}
