package com.inspire11.training.spring.graphql.resolver

import org.springframework.stereotype.Component
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.inspire11.training.spring.graphql.dto.ArticleDto
import com.inspire11.training.spring.graphql.service.ArticleService

@Component
class ArticlesQuery(private val articleService: ArticleService) : GraphQLQueryResolver {
    fun articles() = articleService.findAllArticles()
            .map { ArticleDto(it) }

    fun getArticle(id: Long): ArticleDto? {
        try {
            val article = articleService.findArticle(id)
            return ArticleDto(article)
        } catch(e: Exception) {
            return null
        }
    }
}
