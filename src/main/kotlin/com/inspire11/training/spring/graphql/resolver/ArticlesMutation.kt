package com.inspire11.training.spring.graphql.resolver

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import org.springframework.stereotype.Component
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.inspire11.training.spring.graphql.dto.ArticleDto
import com.inspire11.training.spring.graphql.service.ArticleService
import com.inspire11.training.spring.model.Article
import com.inspire11.training.spring.repository.ArticleRepository
import com.inspire11.training.spring.repository.AuthorRepository
import javax.transaction.Transactional

@Component
class ArticlesMutation(private val articleService: ArticleService) : GraphQLMutationResolver {
    fun createArticle(authorId: Long, article: ArticleDto): ArticleDto? {
        val a = articleService.createArticle(
                authorId = authorId,
                title = article.title!!,
                content = article.content!!
        )
        return ArticleDto(a)
    }

    fun updateArticle(articleId: Long, article: ArticleDto): ArticleDto? {
        val a = articleService.updateArticle(
                articleId = articleId,
                title = article.title!!,
                content = article.content!!
        )
        return ArticleDto(a)
    }

    fun deleteArticle(articleId: Long): Boolean {
        return articleService.deleteArticle(articleId)
    }

    fun changeAuthor(articleId: Long, authorId: Long): ArticleDto {
        val a = articleService.changeAuthor(
                articleId = articleId,
                authorId = authorId
        )
        return ArticleDto(a)
    }
}
