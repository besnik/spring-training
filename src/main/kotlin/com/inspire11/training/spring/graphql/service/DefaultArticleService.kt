package com.inspire11.training.spring.graphql.service

import com.inspire11.training.spring.model.Article
import com.inspire11.training.spring.repository.ArticleRepository
import com.inspire11.training.spring.repository.AuthorRepository
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
@Transactional
class DefaultArticleService(private val articleRepository: ArticleRepository,
                            private val authorRepository: AuthorRepository): ArticleService {
    override fun changeAuthor(articleId: Long, authorId: Long): Article {
        val a = articleRepository.getOne(articleId)
        val author = authorRepository.getOne(authorId)
        a.author = author
        articleRepository.save(a)
        return a
    }

    override fun deleteArticle(articleId: Long): Boolean {
        articleRepository.deleteById(articleId)
        return false
    }

    override fun updateArticle(articleId: Long, title: String, content: String): Article {
        val a = articleRepository.getOne(articleId)
        a.title = title
        a.content = content
        articleRepository.save(a)
        return a
    }

    override fun createArticle(authorId: Long, title: String, content: String): Article {
        val author = authorRepository.getOne(authorId)
        val article = Article(
                title = title,
                content = content
        )
        article.author = author
        articleRepository.save(article)
        return article
    }

    override fun findAllArticles(): List<Article> {
        return articleRepository.findAll()
    }

    override fun findArticle(id: Long): Article {
        return articleRepository.getOne(id)
    }
}
