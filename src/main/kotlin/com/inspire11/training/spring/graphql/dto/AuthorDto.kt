package com.inspire11.training.spring.graphql.dto

import com.inspire11.training.spring.model.Article
import com.inspire11.training.spring.model.Author

data class AuthorDto(
        var id: Long? = null,
        var name: String? = null,
        var surname: String? = null
) {
    var articles: List<Article> = listOf<Article>()

    constructor(author: Author) : this() {
        id = author.id
        name = author.name
        surname = author.surname

    }
}