package com.inspire11.training.spring.graphql.resolver

import org.springframework.stereotype.Component
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.inspire11.training.spring.graphql.dto.AuthorDto
import com.inspire11.training.spring.repository.AuthorRepository

@Component
class AuthorsQuery(private val authorRepository: AuthorRepository) : GraphQLQueryResolver {
    fun authors() = authorRepository.findAll()
            .map { AuthorDto(it) }

    fun getAuthor(id: Long): AuthorDto? {
        try {
            val author = authorRepository.getOne(id)
            return AuthorDto(author)
        } catch(e: Exception) {
            return null
        }
    }
}
